# Documentation

##Table of Contents

* [Getting Started](#getting-started)
	* [Requirements](#requirements)
	* [Configuration](#configuration)
	* [Workflow](#workflow)
		* [Database Schema](#database-schema)
		* [Taxonomy Import](#taxonomy-import)
		* [Site Update Command](#site-update-command)
		* [Provider Agnostic Worker](#provider-agnostic-worker)
		* [Specific Worker](#specific-worker)
		* [Generating the Feed](#generating-the-feed)
* [Feeds Database Structure](#feeds-database-structure)
	* [Taxonomy](#taxonomy)
		* [Table provider](#table-provider)
 		* [Table taxonomy_mapping](#table-taxonomy_mapping)
 		* [Table taxonomy_provider](#table-taxonomy_provider)
 		* [Table taxonomy_worldstores](#table-taxonomy_worldstores)
	* [Feeds Site Account Credentials](#feeds-site-account-credentials)
		* [Table feed_site](#table-feed_site)
		* [Table feed_site_properity_attribute](#table-feed_site_properity_attribute)
			* [Property Reference Values](#property-reference-values)
		* [Table feed_site_properity_value](#table-feed_site_properity_value)
	* [Feeds Generation](#feeds-generation)
		* [Table feeds](#table-feeds)
	* [New Provider Setup](#new-provider-setup)
		* [Creation of Templating Service](#creation-of-templating-service)
		* [Register Templating Service](#register-templating-service)
		* [Feed Configuration](#feed-configuration)
		* [Import Provider Taxonomy](#import-provider-taxonomy)

## Getting Started

### Requirements

The feed engine requirements include a running Redis, Beanstalk and MySQL instances. 

### Configuration

The configuration for the above mentioned requirements is located in two files - for the Redis and Beanstalk it is in **app/config.yml** under the **worldstores_feed_core** key and for the MySQL configuration in **app/parameters.yml** under **parameters**. In the config.yml file, the **feed_list** need need to configure the list of feeds which will be able to generate in the feed engine. More information on what is required for this feed configuration to work view [Feeds Database Structure](#feeds-database-structure) with setting up the feed providers, available sites and upload credentials.

The data could be added directly in the database as referenced in the [Feeds Database Structure](#feeds-database-structure) or via the use of the convenient Doctrine fixtures located in **src/Worldstores/Feed/CoreBundle/Resources/fixtures** which can be loaded in as part of the workflow described next.

### Workflow

#### Database Schema

After installing the feed engine and configuring the requirements, the database schema must be created:

	~/$ php app/console doctrine:schema:update --force

and the fixtures for the feeds and site:

	~/$ php app/console doctrine:fixtures:load --append

#### Taxonomy Import

Then a working taxonomy for all the providers you need feeds generated for must be imported to the feed engine:

	~/$ php app/console worldstores:feed:taxonomy:import filename
	
#### Site Update Command

After these steps we have all the necessary configuration to generate a feed in the system. The steps to do so are as follow. First we need to query the Central Admin API to get a list of all the products and/or categories for a given site. This is done via the site update command and the name of the site. For example to get the list of products for Casafina:

	~/$ php app/console worldstores:feed:site:product:update casafina
	
or the list of categories:

	~/$ php app/console worldstores:feed:site:category:update casafina
	
The command validates the sitename that is given as a parameter, deletes any previously stored information in the Redis server about it and after getting a list of all the active feeds, loads in Beanstalk jobs in the **feed_provider_agnostic_product_data** or **feed_provider_agnostic_category_data** queue. These jobs will be picked up by the agnostic workers in the following step.

#### Provider Agnostic Worker

After the jobs are loaded in Beanstalk the agnostic worker comes in to pick them up and process provider agnostic information about the products or categories. For the products this is done via the following command:

	~/$ php app/console worldstores:feed:worker:product:agnostic

or for the categories via:

	~/$ php app/console worldstores:feed:worker:category:agnostic

The agnostic worker gets in serialized information in the jobs in the queue and runs them through a agnostic service. The service contacts the Central Admin API to get product or category information and store it in provider agnostic keys in Redis under **product-id** or **category-id** (with id being the product or category id). Then it gets all the sites in which each product or category is present and for each of these sites checks if there is an active feed and whether this site has a support to generate products or categories. If all of this checks out then some jobs are put in the **feed_provider_transformed_product_data** or **feed_provider_transformed_category_data** queue for further processing in the specific workers.

#### Specific Worker

The specific workers are executed via:
 
	~/$ php app/console worldstores:feed:worker:product:specific
	
and

	~/$ php app/console worldstores:feed:worker:product:specific

for the products and categories accordingly. They take the information about the products and/or categories per each site and after running it through a templating service it sets templated data for each feed in Redis. The data is then used to generate the feed and upload it.

During the operation of both the agnostic and specific workers a command shows the progress of how the jobs are being completed. The command is available via:

	~/$ php app/console worldstores:beanstalk:show-stats

#### Generating the Feed

The last step in the feed production is the generation of the actual feed off the templated data stored in Redis. This is done via:

	~/$ php app/console worldstores:feed:generate

The category data in Redis will be used in the generation of sitemaps for a given site and can be used via:

	~/$ php app/console worldstores:sitemap:generate

The actual feed and sitemap files are generated and stored in **app/feeds** folder of the Feed Engine. With the upload options set in the last command the files can be uploaded to a preconfigured location and if decided so deleted after the upload.

## Feeds Database Structure

### Taxonomy

The taxonomy part of the Feed Bundle is based on the use of three tables - **provider**, **taxonomy_mapping**, **taxonomy_provider** and **taxonomy_worldstores**.

#### Table provider
The **provider** table provides the relationship between the product categories in the Worldstores sites and the provider's system. The table structure is as follows:

| Field Name         | Field Type   | Description                                                                                          |
| :------------------|:-------------|:-----------------------------------------------------------------------------------------------------|
| id                 | integer (11) | ID of the provider - autoincremented                                                                 |
| name               | varchar (30) | Name of the provider to be used across the system (lower case slug)                                  |
| title              | varchar (10) | Descriptive name of the provider (for use in name field to generate appropriate name)                |
| taxonomy_separator | varchar (50) | Separator for dividing the categories in the category path [Example: Parent Category **>** Category] |
| last_updated       | datetime     | Datetime reference of the last time the taxonomy records were updated in the database                |

#### Table taxonomy_mapping
The **taxonomy_mapping** table provides the relationship between the product categories in the Worldstores sites and the provider's system. The table structure is as follows:

| Field Name              | Field Type   | Description                                                                        |
| :---------------------- |:-------------|:-----------------------------------------------------------------------------------|
| id                      | integer (11) | ID of the mapping - auto-incremented                                               |
| taxonomy_worldstores_id | integer (11) | ID of a products category in WorldStores [Reference - **taxonomy_worldstores.id**] |
| taxonomy_provider_id    | integer (11) | ID of a products category in Provider [Reference - **taxonomy_provider.id**]       |

#### Table taxonomy_provider
The **taxonomy_provider** table holds information about the categories in the providers' systems. Infinite levels of categories are supported through the use of parent_id reference to the parent category. The table structure is as follows:

| Field Name  | Field Type    | Description                                                                          |
| :-----------|:--------------|:-------------------------------------------------------------------------------------|
| id          | integer (11)  | ID of the database record - auto-incremented                                         |
| parent_id   | integer (11)  | ID of the parent category of a given category [Reference - **taxonomy_provider.id**] |
| provider_id | integer (11)  | ID of the Provider of that taxonomy structure [Reference - **provider.id**]          |
| title       | varchar (100) | The name of the category on the provider's system                                    |

#### Table taxonomy_worldstores
The **taxonomy_worldstores** table holds information about the categories in the WorldStores system. Four level of categories are supported through the use of parent_id reference to the parent category. The table structure is as follows:

| Field Name        | Field Type    | Description                                                                             |
| :-----------------|:--------------|:----------------------------------------------------------------------------------------|
| id                | integer (11)  | ID of the database record - auto-incremented                                            |
| parent_id         | integer (11)  | ID of the parent category of a given category [Reference - **taxonomy_worldstores.id**] |
| classification_id | integer (11)  | Unique ID reference of the category for use in the system                               |
| title             | varchar (100) | The name of the category on the WorldStores system                                      |


### Feeds Site Account Credentials

The part of the Feed Bundle which holds account credentials for use in the feeds upload process is available in the following three tables - **feed_site**, **feed_site_property_attribute** and **feed_site_property_value**.

#### Table feed_site
The **feed_site** table holds refenrece information about the site. The table structure is as follows:

| Field Name       | Field Type   | Description                                                                    |
| :----------------|:-------------|:-------------------------------------------------------------------------------|
| id               | integer (11) | ID of the site record - auto-incremented                                       |
| feed_id          | integer (11) | ID of the feed [Reference - **feed.id**]                                       |
| site_id          | integer (11) | ID of a site for which feed is being generated                                 |
| seq              | integer (11) | Sequence value to be used in display of the feeds in the Central Admin         |
| flow_control     | tinyint (1)  | Boolean value of the flow control applied to the feed - 1 for on, 0 for off    |
| product_support  | tinyint (1)  | Boolean value of the support for products in the feed - 1 for on, 0 for off    |
| category_support | tinyint (1)  | Boolean value of the support for categories in the feed - 1 for on, 0 for off  |
| last_updated     | datetime     | Datetime reference of the last time the feed was updated for a particular site |

#### Table feed_site_properity_attribute
The **feed_site_property_attribute** table holds information about the properties used in the authentication and upload location of the feeds process . The table structure is as follows:

| Field Name         | Field Type    | Description                                       |
| :------------------|:--------------|:--------------------------------------------------|
| id                 | integer (11)  | ID of the property record - auto-incremented      |
| feed_property_name | varchar (255) | Name of the property [See reference values below] |

###### Property Reference Values

* **upload_protocol** (**id**: 1) - Protocol to be used to upload the feed to the Provider's system (example: ftp)
* **upload_hostname** (**id**: 2) - Host name of the Provider's system to upload feeds to (example: uploads.google.com)
* **upload_port** (**id**: 3) - Port on the Provider's system to upload feeds to (example: 21)
* **upload_auth_username** (**id**: 4) - Username used to authenticate on the Provider's system when uploading the feeds
* **upload_auth_password** (**id**: 5) - Password used to authenticate on the Provider's system when uploading the feeds
* **upload_path** (**id**: 6) - Path to upload the feed to on the Provider's system (example: /)

#### Table feed_site_properity_value
The **feed_site_property_value** table holds information about the properties. The table structure is as follows:

| Field Name                    | Field Type    | Description                                                               |
| :-----------------------------|:--------------|:--------------------------------------------------------------------------|
| id                            | integer (11)  | ID of the value record - auto-incremented                                 |
| feed_site_id                  | integer (11)  | ID of the site in the feed [Reference - **feed_site.id**]                 |
| feed_property_id              | integer (11)  | ID of the property used [Reference - **feed_site_property_attribute.id**] |
| feed_property_attribute_value | varchar (255) | Value for the property used in the upload process                         |
| last_updated                  | datetime      | Datetime reference of the last time the record was updated                |


### Feeds Generation

The generation of the feed has records in **feeds**.

#### Table Feeds
The **feeds** table contains generic details about the feeds generated. The table structure is as follows:

| Field Name      | Field Type    | Description                                                                       |
| :---------------|:--------------|:----------------------------------------------------------------------------------|
| id              | integer (11)  | ID of the property record - auto-incremented                                      |
| provider_id     | integer (11)  | ID of the provider to which the feed is uploaded [Reference - **provider.id**]    |
| name            | varchar (100) | Name of the feed to be used in the system (lower case slug))                      |
| title           | varchar (100) | Descriptive name of the feed (for use in name field to generate appropriate name) |
| archive_support | varchar (10)  | Field contains format of archive support if one is present [Example: zip]         |
| last_updated    | datetime      | Datetime reference of the last time the record was updated                        |


### New Provider Setup

Below are instructions on setting up a new provider in the Feeds Bundle. There are three distinct steps to it.

#### Creation of Templating Service

In order to process information about the products and properly add it to a feed we need a templating service which will take care of the processing and checks. The service needs to have the following characteristics:

1. It must implement **Worldstores\Feed\CoreBundle\Service\Generator\Templating** Interface
2. It must extend the abstract class **Worldstores\Feed\CoreBundle\Service\Generator\Templating\AbstractTemplating**
3. Namespace of the service class should be **Worldstores\Feed\CoreBundle\Service\Generator\Templating**
4. The class must have access to the \Twig_Environment (either by setter or constructor injection)
5. The class must have access to the ProxyModel (**Worldstores\Feed\CoreBundle\Service\Generator\ProxyModel**) provided in the bundle
6. The **render()** method from the Templating interface contract must return a string with a rendered template i.e feed XML using the Twig service and the ProxyModel
7. The Twig template to render to should be located in Worldstores\Feed\CoreBundle\Resources\views\feed\\**provider**\item.twig.xml where **provider** is the slug of the provider as registered in DB - **provider.name**

#### Register Templating Service

Once the templating service is created it needs to be registered to the Symfony container. To do that we add it to the **Resrouces/config/services.xml** of the bundle. The service must be tagged with a name set to **worldstores_feed_core.feed.templating** and value set to a slug like **provider-product-feed** where **provider** is the slug of the provider (referenced in DB - **feed.name**). This will later be used in the **app/config/config.xml** for setting up the feeds.

#### Feed Configuration

In **app/config/config.xml** there is a configuration setting under **worldstores_feed_core** called **feed_list**. In this setting we will add an array of the feed for the new provider. There are three parameters:

1. **name** - must be the same slug as the one used in the value of the tagged templating service i.e **provider-product-feed**
2. **proxy** - a service definition of the Proxy Model DTO
3. **is_active** - a boolean for whether the feed should be on or off (if skipped the default value is **true** i.e **on**)

Example setting:

    feed_list:
      - { name: "google-product-feed", proxy: "worldstores_feed_core.service.generator.proxy_model.concrete_dto"}
      
#### Import Provider Taxonomy

When everything is set up with the provider and the feed we need a taxonomy file to yield mapping data between the provider's category system and the categories in Worldstores. The file is an Excel file and is imported into the system using a Symfony CLI command **worldstores:feed:taxonomy:import** as follows:

	~/$ php app/console worldstores:feed:taxonomy:import filename

where **filename** is the location to the taxonomy file.

This command will populate the following tables - **provider**, **taxonomy_mapping**, **taxonomy_provider** and **taxonomy_worldstores**.

